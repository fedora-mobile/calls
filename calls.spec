Name:		calls
Version:	41.~1alpha
Release:	1%{?dist}
Summary:	A phone dialer and call handler
License:	GPLv3+ and MIT
URL:		https://gitlab.gnome.org/GNOME/calls
Source0:	https://gitlab.gnome.org/devrtz/calls/-/archive/wip/sdp-session-ip/calls-wip-sdp-session-ip.tar.gz

	
BuildRequires:	gcc	
BuildRequires:	meson	
BuildRequires:	cmake
BuildRequires:	gcc-c++
	
	
BuildRequires:	pkgconfig(libcallaudio-0.1)	
BuildRequires:	pkgconfig(gobject-2.0)	
BuildRequires:	pkgconfig(glib-2.0) >= 2.50.0	
BuildRequires:	pkgconfig(gtk+-3.0)	
BuildRequires:	pkgconfig(libhandy-1) >= 1.0.0	
BuildRequires:	pkgconfig(gsound)	
BuildRequires:	pkgconfig(libpeas-1.0)	
BuildRequires:	pkgconfig(gom-1.0)	
BuildRequires:	pkgconfig(libebook-contacts-1.2)	
BuildRequires:	pkgconfig(folks)	
BuildRequires:	pkgconfig(mm-glib)	
BuildRequires:	pkgconfig(libfeedback-0.0)	
BuildRequires:	pkgconfig(gstreamer-1.0)	
BuildRequires:	gstreamer1-plugins-good-gtk	
BuildRequires:	sofia-sip-glib-devel 
	
BuildRequires:	desktop-file-utils	
BuildRequires:	/usr/bin/xvfb-run	
BuildRequires:	/usr/bin/xauth	
BuildRequires:	libappstream-glib 
	
Requires: hicolor-icon-theme
	
 
	
%description	
A phone dialer and call handler. 
	
%prep	
%setup -q -n calls-wip-sdp-session-ip
	
%build	
%meson	
%meson_build 
	
%install	
%meson_install	
%find_lang %{name} 
	
%check	
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/org.gnome.Calls.metainfo.xml 
	
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Calls.desktop
		
# Some tests are failing in the build environment, so we manually just run a handful for now.	
LC_ALL=C.UTF-8 xvfb-run sh <<'SH'	
%meson_test manager plugins	
SH
	
%files -f %{name}.lang	
%{_sysconfdir}/xdg/autostart/org.gnome.Calls-daemon.desktop	
%{_bindir}/gnome-%{name} 
	
%dir %{_libdir}/calls	
%dir %{_libdir}/calls/plugins	
%dir %{_libdir}/calls/plugins/mm	
%dir %{_libdir}/calls/plugins/dummy	
%dir %{_libdir}/calls/plugins/sip 
	
%{_libdir}/calls/plugins/mm/libmm.so	
%{_libdir}/calls/plugins/mm/mm.plugin	
%{_libdir}/calls/plugins/dummy/dummy.plugin	
%{_libdir}/calls/plugins/dummy/libdummy.so	
%{_libdir}/calls/plugins/sip/libsip.so	
%{_libdir}/calls/plugins/sip/sip.plugin 
	
# ofono is retired so we exclude the plugins 4/24/2020	
%exclude %{_libdir}/calls/plugins/ofono/libofono.so	
%exclude %{_libdir}/calls/plugins/ofono/ofono.plugin
	
%{_datadir}/glib-2.0/schemas/org.gnome.Calls.gschema.xml	
%{_datadir}/applications/org.gnome.Calls.desktop	
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Calls.svg	
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Calls-symbolic.svg	
%{_datadir}/metainfo/org.gnome.Calls.metainfo.xml 
	
%doc README.md	
%license COPYING

%changelog
* Thu Sep 30 2021 Tor - 41.~1alpha-1
- Test calls-wip-sdp-session-ip

* Mon May 18 2020 Torrey sorensen <sorensentor@tuta.io> - 0.1.5-1
- Owning the directories for calls, plugins, and mm. 
- Update version 0.1.5. 
- Adding MIT to License
- Upstream changed "appdata" to "metainfo"

* Fri Apr 24 2020 Torrey Sorensen <sorensentor@tuta.io> - 0.1.4-1
- Updating version 0.1.4. Fixing comments from review regarding ofono and appdata.

* Fri Mar 27 2020 Torrey Sorensen <sorensentor@tuta.io> - 0.1.3-1 
- Updating version 0.1.3. Adding tests

* Wed Mar 25 2020 Torrey Sorensen <sorensentor@tuta.io> - 0.1.2-2
- Adding license and meson_test. Remove buildid

* Sun Mar 08 2020 Torrey Sorensen <sorensentor@tuta.io> - 0.1.2-1
- Initial packaging
